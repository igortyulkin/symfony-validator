<?php

namespace Fmedia\Validator;

interface ValidatorInterface
{
    /**
     * @param mixed $object
     * @throws ValidationException
     */
    public function validate($object): void;
}
