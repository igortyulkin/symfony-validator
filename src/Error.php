<?php

namespace Fmedia\Validator;

class Error
{
    /**
     * @var string
     */
    private $propertyPath;

    /**
     * @var string
     */
    private $message;

    public function __construct(string $propertyPath, string $message)
    {
        $this->propertyPath = $propertyPath;
        $this->message = $message;
    }

    public function getPropertyPath(): string
    {
        return $this->propertyPath;
    }

    public function getMessage(): string
    {
        return $this->message;
    }
}
