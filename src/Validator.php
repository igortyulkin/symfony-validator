<?php

namespace Fmedia\Validator;

use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface as SymfonyValidatorInterface;

class Validator implements ValidatorInterface
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(SymfonyValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function validate($object): void
    {
        $errors = $this->validator->validate($object);

        if ($errors->count() > 0) {
            throw new ValidationException($this->assembleErrors($errors));
        }
    }

    private function assembleErrors(ConstraintViolationListInterface $violationList): array
    {
        $errors = [];
        /** @var ConstraintViolationInterface $item */
        foreach ($violationList as $item) {
            $errors[] = new Error($item->getPropertyPath(), $item->getMessage());
        }

        return $errors;
    }
}
