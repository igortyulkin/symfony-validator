<?php

namespace Fmedia\Validator;

use Exception;

class ValidationException extends Exception
{
    /**
     * @var Error[]
     */
    private $errors;

    public function __construct(array $errors)
    {
        parent::__construct("Validation error", 0);
        $this->errors = $errors;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }
}
